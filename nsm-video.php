<?php
/*
Plugin Name:  NSM - Video
Description:  NSM - Video.
Version:      1.0.0
Author:       Marlow Perceval
License:      MIT License
*/

add_action('init', 'create_nsm_video', 0);
function create_nsm_video() {

	register_post_type('nsm_video',
		array(
			'labels' => array(
				'name' => __('Videos'),
				'singular_name' => __('Video'),
				'menu_name' => __('Videos'),
				'all_items' => __('All Videos'),
				'add_new_item' => __('Add New Video'),
				'edit_item' => __('Edit Video'),
				'new_item' => __('New Video'),
				'view_item' => __('View Video'),
				'search_items' => __('Search Videos'),
				'not_found' => __('No videos found'),
				'not_found_in_trash' => __('No videos found in Trash'),
			),
			'public' => false,
			'show_ui' => true,
			'menu_position' => 20,
			'menu_icon'   => 'dashicons-media-video',
			'supports' => array(
				'title',
				'thumbnail',
				'page-attributes'
			)
		)
	);
}